# rss.fullstack.llc

RSS Feeds combined from many sources into single feeds.  For digesting into Social media curation tools.

Prototyping functionality to automate away content-studio actions and feeder/feedspot


Based off public no-license-attribution code: http://github.com/awocallaghan/node-rss-combiner
