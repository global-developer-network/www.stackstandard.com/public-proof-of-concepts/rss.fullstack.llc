var express = require('express')
var app = express()

app.use('/static', express.static('public'))

app.get('/', function (req, res) {
  res.set('Content-Type', 'application/rss+xml');

  fs = require('fs')
  fs.readFile('example/xml/tech-example.xml', 'utf8', function (err,data) {
    if (err) {
      return console.log(err);
    }
    console.log(data);
    res.send(data);
  });
  updateFeed();




})

var updateFeed = function(){
  var RSSCombiner = require('./lib');
  var fs = require('fs');

  var techFeedConfig = {
      size: 20,
    feeds: [
        'https://rss.feedspot.com/folder/4hLLtGMb5g==/rss/rsscombiner',
        'https://rss.feedspot.com/folder/4hLLtGIi5g==/rss/rsscombiner',
        'https://rss.feedspot.com/folder/4hLGuWIe5Q==/rss/rsscombiner'
    ],
      pubDate: new Date()
  };

  RSSCombiner(techFeedConfig)
      .then(function (feed) {
          var xml = feed.xml({ indent: true });
          fs.writeFile('example/xml/tech-example.xml', xml, function (err) {
              if (err) {
                  console.error(err);
              } else {
                  console.log('Tech feed written');
              }
          });

        });
};

app.listen(3000, function () {

  console.log('RSS Feed listening on port 3000!')
})
